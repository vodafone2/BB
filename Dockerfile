# This file is a template, and might need editing before it works on your project.
FROM ruby:2.5-alpine

# Edit with nodejs, mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN apk --no-cache add nodejs postgresql-client tzdata

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock .
# Install build dependencies - required for gems with native dependencies
RUN apk add --no-cache --virtual build-deps build-base postgresql-dev && \
  bundle install && \
  apk del build-deps

COPY . .

# For Sinatra
#EXPOSE 4567
#CMD ["ruby", "./config.rb"]

# For Rails
ENV PORT 3000
EXPOSE 3000
CMD ["bundle", "exec", "rails", "server"]

adm@DESKTOP-H6ALMJI MINGW64 /c/software/DevOps/bb (master)
$ history
    1  dir
    2  dir/p
    3  dir/p
    4  cls
    5  clear
    6  cd software
    7  cd:software
    8  cd cd software
    9  cd\software
   10  cd c:\software
   11  cd DevOps
   12  git clone https://gitlab.com/vodafone2/BB.git
   13  pwd
   14  ll
   15  cd bb
   16  dir
   17  ll
   18  ll
   19  git status
   20  clear
   21  git status
   22  git add --all
   23  git status
   24  git commit -m "VD-8 adding sample code forDemo"
   25  git config --global user.name"xxxxxx"
   26  git config --global user.name "xxxxxx"
   27  git config --global user.email "xxxxx.xxx@gmail.com"
   28  git commit -m "VD-8 adding sample code forDemo"
   29  clear
   30  git remote -v
   31  git push origin master
   32  git add --all
   33  git commit -m "VD-8 adding sample code forDemo"
   34  git push origin master
   35  git push origin master
   36  history
